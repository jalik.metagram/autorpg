package autorpg;

import java.io.IOException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This behaviour send and receive messages containing visual informations.
 * 
 * Visual informations are represented by VisualInfo class which is serialized and sent as the content of the message.
 * An agent can see visual informations only from agents in the same Zone.
 * @see VisualInfo 
 */
class VisualInfoBehaviour extends CyclicBehaviour {

    //Generated serial ID
    private static final long serialVersionUID = 5692237397945191665L;

	//Ontology is used as the category of the message
    public static final String ONTOLOGY = "visual"; 

    public void action() 
    {
        //Try to receive "visual info" message
        ACLMessage message = myAgent.receive(MessageTemplate.MatchOntology(VisualInfoBehaviour.ONTOLOGY));
        if (message != null) 
        {
            Character myCharacter = (Character) myAgent;

            //If another agent ask for visual informations
            if (message.getPerformative() == ACLMessage.REQUEST)
            {
                try 
                {
                    //Reply with my visual informations (content is serialized VisualInfo class)
                    ACLMessage reply = message.createReply();
                    VisualInfo info = new VisualInfo(myCharacter);
                    reply.setPerformative(ACLMessage.CONFIRM);
                    reply.setContentObject(info);
                    myAgent.send(reply);
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
            //If I receive visual infos from another agent
            else if (message.getPerformative() == ACLMessage.CONFIRM)
            {
                try 
                {
                    //deserialize visual infos and add it to my memory
                    VisualInfo info = (VisualInfo) message.getContentObject();
                    myCharacter.addVisualInfo(message.getSender(), info, System.currentTimeMillis());
                } 
                catch (UnreadableException e) 
                {
                    e.printStackTrace();
                }
            }
        }
        //Wait for another "visual infos" message
        block();
    }
}