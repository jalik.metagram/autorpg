package autorpg;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * Receive a notification message when this agent killed another agent.
 */
public class KilledNotificationBehaviour extends CyclicBehaviour {

	//Generated Serial ID
	private static final long serialVersionUID = -3648813992950667889L;

	//Ontology is used as the category of the message
	public static final String ONTOLOGY = "isDead";

	//Message templates are used to filter messages by ontogy and performative
	private final MessageTemplate performativeTemp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
	private final MessageTemplate ontologyTemp = MessageTemplate.MatchOntology(KilledNotificationBehaviour.ONTOLOGY);

	@Override
	public void action() 
	{
		//Try to receive the message
		ACLMessage message = myAgent.receive(MessageTemplate.and(performativeTemp, ontologyTemp));

		if (message != null) 
		{
            Character myChar = (Character) myAgent;
			
			//Get the name of the dead agent
			AID deadAgent = message.getSender();
			myChar.notifyKilled(deadAgent);

			//Update console
			StartAutoRPG.INSTANCE.updateConsoleMessage(myAgent.getLocalName() + " killed " + deadAgent.getLocalName());
		}
		//Wait for another message
        block();
	}
}
