package autorpg;

import jade.core.AID;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;
import java.util.List;

public class Healer extends Character {

	/**
	 * Generated Serial
	 */
	private static final long serialVersionUID = -8882898645297783006L;
	
	@Override
	protected void setup() {
		super.setup();
		maxHp = 150;
		hp = maxHp;
		abilities.put("heal", new Ability(2000, 30, 0, 1, 1, TARGET.ALLIES));
		abilities.put("aura", new Ability(5000, 100, 0, -1, -1, TARGET.ALLIES));
	}

	@Override
	public char getClassDescriptor() {
		return 'H';
	}

	@Override
	public void startGame() {
		super.startGame();
	}

	@Override
	public ZONE computeZone() { // look for last ally in memory
		ZONE[] adjacentZones = currentZone.getNext();
		List<CharacterInfo> closeOnes = myMemory.getClosestAlly(this);
		if(closeOnes.isEmpty()){
			return super.computeZone();
		} else {
			if(Arrays.asList(adjacentZones).contains(closeOnes.get(0).getLastZone())){
				return closeOnes.get(0).getLastZone();
			}
		}
		return super.computeZone();
	}

	@Override
	public AID[] computeEnemyTarget(int nbTargets) {
		// TODO Auto-generated method stub
		return super.computeEnemyTarget(nbTargets);
	}


	// should choose weakest ally as target
	@Override
	public AID[] computeAllyTarget(int nbTargets) {
		if(nbTargets == -1)
			return nearAllies;
		ArrayList<AID> alliesPossible = new ArrayList<AID>();
		for(int i = 0 ; i < nearAllies.length ; ++i) {
			alliesPossible.add(nearAllies[i]);
		}
		AID[] toReturn = new AID[Math.min(nbTargets, alliesPossible.size())];
		CharacterInfo charMinInfo = myMemory.getLastCharactersSeen().get(alliesPossible.get(0));
		float weakestPercentage = percentageHPCurrent(alliesPossible.get(0));
		int indexWeakPercent = 0;
		for(int i = 0; i < toReturn.length; ++i){
			for(int j = 0; j < alliesPossible.size(); ++j){
				float percentHP = percentageHPCurrent(alliesPossible.get(j));
				if(percentHP < weakestPercentage){
					indexWeakPercent = j;
				}
			}
			toReturn[i] = alliesPossible.get(indexWeakPercent);
			alliesPossible.remove(toReturn[i]);
		}
		return toReturn;
	}

	public int howManyHPLost(AID charAID){
		if(charAID == null){
			return 0;
		} else {
			CharacterInfo charInfo = myMemory.getLastCharactersSeen().get(charAID);
			if(charInfo == null)
				return 0;
			else
				return charInfo.getMaxHP() - charInfo.getHp();
		}
	}

	public float percentageHPCurrent(AID charAID){
		if(charAID == null){
			return 1;
		} else {
			CharacterInfo charInfo = myMemory.getLastCharactersSeen().get(charAID);
			if(charInfo == null)
				return 1;
			else{
				float ret = (float)(charInfo.getHp()) / charInfo.getMaxHP();
				return ret;
			}
		}
	}

	public int mostHPLostAlliesInZone(){
		int ret = 0;
		for(int i = 0; i < nearAllies.length; ++i){
			int hpLost = howManyHPLost(nearAllies[i]);
			if(hpLost > ret)
				ret = hpLost;
		}
		return ret;
	}

	@Override
	public void computeNextAbility() {		
		int mostHPLost = mostHPLostAlliesInZone();
		if(nearAllies.length > 1){ // allies in zone other than self
			if (mostHPLost >= 80) { // if ally or self very hurt
				changeNextAbility("aura");
			} else { // not that hurt
				if(mostHPLost >= 40){ // a little hurt
					if(nearAllies.length > 2){
						int rand = new Random().nextInt(4);
						if(rand == 0){ // 1/4 chance : Heal
							changeNextAbility("heal");
						} else { // 3/4 chance : aura
							changeNextAbility("aura");
						}
					} else {
						changeNextAbility("heal");						
					}
				} else { // really not that hurt
					int rand = new Random().nextInt(2);
					if(rand == 0){ // 1/2 chance : Heal or talk
						if(mostHPLost > 0)
							changeNextAbility("heal");
						else
							changeNextAbility("share"); // not many chances, other share with them often
					} else { // 1/2 chance : move
						changeNextAbility("move");
					}
				}
			}
		} else { // no ally in zone
			if(nearEnemies.length > 0){ // enemies in zone				
				changeNextAbility("move"); // flee
			} else { // alone in zone
				if(mostHPLost >= 50){ // a little hurt
					changeNextAbility("heal");
				} else { // 3/4 chance : aura		
					changeNextAbility("move");					
				}
			}
		}
	}
}
