package autorpg;

import jade.core.behaviours.CyclicBehaviour;


/**
 * This behaviour updates the console at a fixed time frequency
 */
public class UpdateConsoleBehaviour extends CyclicBehaviour {
	
	//Generated serial version ID
	private static final long serialVersionUID = 4847346137726627612L;

	@Override
	public void action() 
	{
		//1 time per second
		block((long) (1000 * StartAutoRPG.getGameSpeed()));

		//Update console
		StartAutoRPG game = (StartAutoRPG) myAgent;
		game.updateConsole();
	}

}
