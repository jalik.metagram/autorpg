# autorpg

Important ! Pensez à ajouter votre dossier où vous clonez votre git au class path.

1/ cd monDossier (moi je fais ça dans jade/src)   
2/ git clone [...]autorpg.git   
3/ bouton windows > taper "env" > cliquer sur "Modifier Variables d'environnement" > Modifier class path > Ajouter "C:/[...]/monDossier/autorpg/" (moi c'est jade/src/autorpg)   
4a/ lancer le .bat (il compile + lance jade)   
OU   
4b/ lancer java jade.Boot -GUI dans un cmd si vous avez pas envie de recompiler   
5/ tenter d'ajouter un agent, dans la liste il doit y avoir "autorpg.TestAgent"   
