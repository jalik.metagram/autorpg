package autorpg;

import java.io.IOException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 * Receive attack damage messages from another agent.
 * 
 * When another agent attacks this agent, he sends a message containing attack damages (a number).
 * This agent will lose a number of hit points (or life points) equals to attack damages sent.
 */
class ReceiveDamageBehaviour extends CyclicBehaviour {
	
	//Generated Serial ID
	private static final long serialVersionUID = -432190400520766055L;
	
	//Ontology is used as the category of the message
	public static final String ONTOLOGY = "dmg";

	//Message templates are used to filter messages by ontogy and performative
	private final MessageTemplate performativeTemp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
	private final MessageTemplate ontologyTemp = MessageTemplate.MatchOntology(ReceiveDamageBehaviour.ONTOLOGY);

	public void action() 
	{
		//Try to receive the message
        ACLMessage message = myAgent.receive(MessageTemplate.and(performativeTemp, ontologyTemp));
		if (message != null) 
		{
			Character myChar = (Character) myAgent;
			
			//Take damages (lose hit points)
			myChar.takeDamage(Integer.parseInt(message.getContent()));
			
			//If this agent dies (no more hit points)
			if(myChar.getHp() <= 0) 
			{
				//Send a message to the attacker agent to notify that I'm dead
        		ACLMessage deadMsg = new ACLMessage(ACLMessage.INFORM);
        		deadMsg.setOntology(KilledNotificationBehaviour.ONTOLOGY);
				deadMsg.addReceiver(message.getSender());
				myAgent.send(deadMsg);
				myAgent.doDelete();
			}
			else 
			{
				//Add a console message
        		StartAutoRPG.INSTANCE.updateConsoleMessage(myAgent.getLocalName() + " took " + message.getContent() + " damage");
        	}
		}
		//Wait for another message
        block();
    }
  }