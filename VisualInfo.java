package autorpg;

import java.io.Serializable;


/**
 * A class describing visual informations of an agent, that can be seen by other agents.
 * Visual informations are sent by message between agents in the same zone.
 * @see VisualInfoBehaviour
 */
public class VisualInfo implements Serializable {
    
    //Generated serial version ID
    private static final long serialVersionUID = 2440506388701324988L;

    //Hit points (or life points) when they are equals to 0 the agent dies
    public int hp;
    
    //Maximum hit points
    public int maxHp;

    //The zone where the agent is
    public ZONE zone;

    //The team the agent belongs to
    public TARGET team;

    //A letter describing the class of the agent (Mage, Healer, Barbarian)
    public char classDescriptor;
    
    //Construct a VisualInfo from a character
    public VisualInfo(Character character) 
    {
        this.hp = character.getHp();
        this.maxHp = character.getMaxHp();
        this.classDescriptor = character.getClassDescriptor();
        this.zone = character.getCurrentZone();
        this.team = character.getTeam();
    }
}
