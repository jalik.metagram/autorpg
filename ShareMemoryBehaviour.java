package autorpg;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 * This behaviours receive the memory of an ally agent and reply with its own memory.
 * 
 * The memory is sent by message which content is serialized Memory class.
 * @see Memory
 */
public class ShareMemoryBehaviour extends CyclicBehaviour {

    //Generated serial version ID
    private static final long serialVersionUID = 410231584848964718L;

    //Ontology is used as the category of the messages
    public static final String ONTOLOGY = "share";

    public void action() 
    {
        //Try to receive a "share memory" message
        ACLMessage message = myAgent.receive(MessageTemplate.MatchOntology(ONTOLOGY));

        if (message != null) 
        {
            try 
            {
                //Receive the memory of the sender agent and update my memory with it
                Memory memory = (Memory) message.getContentObject();
                ((Character) myAgent).getMyMemory().updateGetInfoFromAlly(memory);

                //If another agent initiated the sharing
                if (message.getPerformative() == ACLMessage.REQUEST) 
                {
                    //Reply with my memory
                    ACLMessage reply = message.createReply();
                    Character myCharacter = (Character) myAgent;
                    reply.setPerformative(ACLMessage.CONFIRM);
                    reply.setContentObject(myCharacter.getMyMemory());
                    myAgent.send(reply);
                }
                else if (message.getPerformative() == ACLMessage.CONFIRM)
                {
                    //do nothing
                } 
            } catch (Exception e) 
            {
                System.err.println("DEBUG = " + ((Character) myAgent).getMyMemory());
                e.printStackTrace();
            }
        }
        //Wait for other "share memory" messages
        block();
    }
}
