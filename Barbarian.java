package autorpg;

import jade.core.AID;

import java.lang.annotation.Target;
import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;

public class Barbarian extends Character {

	/**
	 * Generated serial
	 */
	private static final long serialVersionUID = 8602980098797295823L;
	
	@Override
	protected void setup() {
		super.setup();
		maxHp = 500;
		hp = maxHp;
		abilities.put("axe", new Ability(1500, 25, 0, 0, 1, "Attacking with axe",TARGET.ENEMIES));
		abilities.put("headbang", new Ability(2000, 15, 1, 0, 1, "Attacking with head", TARGET.ENEMIES));
	}

	@Override
	public char getClassDescriptor() {
		return 'B';
	}
	
	@Override
	public void startGame() {
		super.startGame();
	}

	@Override
	public ZONE computeZone() {		 // look for enemies in memory
		ZONE[] adjacentZones = currentZone.getNext();
		List<CharacterInfo> closeOnes = myMemory.getClosestEnemy(this);
		if(closeOnes.isEmpty()){
			return super.computeZone();
		} else {
			if(Arrays.asList(adjacentZones).contains(closeOnes.get(0).getLastZone())){
				return closeOnes.get(0).getLastZone();
			}
		}
		return super.computeZone();
	}

	// targets weakest enemy
	@Override
	public AID[] computeEnemyTarget(int nbTargets) {		
		if(nbTargets == -1)
			return nearEnemies;
		ArrayList<AID> enemiesPossible = new ArrayList<AID>();
		for(int i = 0 ; i < nearEnemies.length ; ++i) {
			enemiesPossible.add(nearEnemies[i]);
		}
		AID[] toReturn = new AID[Math.min(nbTargets, enemiesPossible.size())];
		CharacterInfo charMinInfo = myMemory.getLastCharactersSeen().get(enemiesPossible.get(0));
		float weakestPercentage = percentageHPCurrent(enemiesPossible.get(0));
		int indexWeakPercent = 0;
		for(int i = 0; i < toReturn.length; ++i){
			for(int j = 0; j < enemiesPossible.size(); ++j){
				float percentHP = percentageHPCurrent(enemiesPossible.get(j));
				if(percentHP < weakestPercentage){
					indexWeakPercent = j;
				}
			}
			toReturn[i] = enemiesPossible.get(indexWeakPercent);
			enemiesPossible.remove(toReturn[i]);
		}
		return toReturn;
	}

	public float percentageHPCurrent(AID charAID){
		if(charAID == null){
			return 1;
		} else {
			CharacterInfo charInfo = myMemory.getLastCharactersSeen().get(charAID);
			if(charInfo == null)
				return 1;
			else{
				float ret = (float)(charInfo.getHp()) / charInfo.getMaxHP();
				return ret;
			}
		}
	}

	@Override
	public AID[] computeAllyTarget(int nbTargets) {
		return super.computeAllyTarget(nbTargets);
	}

	// called if there is at least one enemy
	public boolean isThereABarbarian(){
		for(int i = 0; i < nearEnemies.length; ++i){
			CharacterInfo enemy = myMemory.getLastCharactersSeen().get(nearEnemies[i]);
			if(enemy == null) //Peut arriver rarement
				continue;
			if(enemy.getClassDescriptor() == 'B'){
				return true;
			}
		}
		return false;
	}

	@Override
	public void computeNextAbility() {
		myMemory.getLastCharactersSeen().get(nearAllies[0]).getHp();
		if(nearEnemies.length > 0)
			if(isThereABarbarian()){ // motivé ! taper à la hache
				changeNextAbility("axe");
			} else { // des gens pas en slip de guerre et sans hache : pas motivé, coup de tête
				changeNextAbility("headbang");
			}
		else {
			if(nearAllies.length > 1){
				int rand = new Random().nextInt(2);
				if(rand == 1){
					changeNextAbility("share");
				} else {
					changeNextAbility("move"); // move to closest enemy if possible
				}
			} else {
				changeNextAbility("move");
			}
		}
	}
}
