package autorpg;

import java.io.Serializable;

public class CharacterInfo implements Serializable{
	
	/**
	 * Generated serial
	 */
	private static final long serialVersionUID = -242672585636360909L;
	
	private final int hp;
	private final int maxHP;
	private final ZONE lastZone;
	private final TARGET team;
	private final long timeInfo;
	private final char classDescriptor;
	
	public CharacterInfo(VisualInfo info, long timeSeen) {
		hp = info.hp;
		maxHP = info.maxHp;
		lastZone = info.zone;
		team = info.team;
		timeInfo = timeSeen;
		classDescriptor = info.classDescriptor;
	}
	
	public int getHp() {
		return hp;
	}

	public int getMaxHP() {
		return maxHP;
	}

	public ZONE getLastZone() {
		return lastZone;
	}

	public TARGET getTeam() {
		return team;
	}

	public long getTimeInfo() {
		return timeInfo;
	}
	
	public char getClassDescriptor() {
		return classDescriptor;
	}
}
