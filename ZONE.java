package autorpg;

import java.util.Random;

public enum ZONE {
	///Connects to NORTH and SOUTH
	EAST,
	///Connects to NORTH and SOUTH
	WEST,
	///Connects to EAST and WEST
	NORTH,
	///Connects to EAST and WEST
	SOUTH;
	
	public ZONE[] getNext() {
		ZONE[] toReturn = new ZONE[2];
		switch(this) {
			case EAST:
			case WEST:
				toReturn[0] = NORTH;
				toReturn[1] = SOUTH;
				break;
			default:
				toReturn[0] = EAST;
				toReturn[1] = WEST;
				break;
		}
		return toReturn;
	}
	
	public ZONE getRandomNext() {
		ZONE[] toChoose = getNext();
		return toChoose[new Random().nextInt(toChoose.length)];
	}
	
	public int distance(ZONE other) {
		if(other.equals(this))
			return 0;
		switch(this) {
			case EAST:
			case WEST:
				if(other.equals(NORTH) || other.equals(SOUTH))
					return 1;
				else
					return 2;
			default:
				if(other.equals(WEST) || other.equals(EAST))
					return 1;
				else
					return 2;
				
		}
	}
}
