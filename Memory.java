package autorpg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jade.core.AID;

/**
 * Memory is a class representing the memory of a character. 
 * It stores information about other character that have been seen or killed.
 * It also stores the time and the zone of these observations.
 * Memory can be shared between allies characters.
 * @see ShareMemoryBehaviour
 * @see CharacterInfo
 */
public class Memory implements Serializable 
{
	/**
	 * Generated serial ID
	 */
	private static final long serialVersionUID = -6579643533423160185L;
	
	/**
	 * Map which give all CharacterInfo of each zone
	 * @see CharacterInfo
	 */
	private Map<ZONE, List<CharacterInfo>> lastStateZones;

	/**
	 * Map which give the CharacterInfo of each Agent
	 * @see CharacterInfo
	 */
	private Map<AID, CharacterInfo> lastCharactersSeen;

	/**
	 * A map storing if characters are dead
	 */
	private Map<AID, Boolean> deadCharacters;
	

	public Map<AID, Boolean> getDeadCharacters() {
		return deadCharacters;
	}

	/**
	 * @param charac the AID of the character
	 * @return true if I am sure he is dead, false in other cases
	 */
	public boolean iKnowHesDead(AID charac) {
		Boolean isDead = deadCharacters.get(charac);
		if(isDead == null)
			return false;
		return isDead.booleanValue();
	}
	
	public Map<ZONE, List<CharacterInfo>> getLastStateZones() {
		return lastStateZones;
	}

	public Map<AID, CharacterInfo> getLastCharactersSeen() {
		return lastCharactersSeen;
	}

	public Memory() {
		lastCharactersSeen = new HashMap<>();
		lastStateZones = new HashMap<>();
		lastStateZones.put(ZONE.NORTH, new ArrayList<>());
		lastStateZones.put(ZONE.SOUTH, new ArrayList<>());
		lastStateZones.put(ZONE.EAST, new ArrayList<>());
		lastStateZones.put(ZONE.WEST, new ArrayList<>());
		deadCharacters = new HashMap<>();
		
	}
	

	/**
	 * Stores that an agent has been seen.
	 * @param aid AID of this agent
	 * @param toRemember Character infos to remember
	 */
	private void seeSomeone(AID aid, CharacterInfo toRemember) {
		CharacterInfo oldInfo = lastCharactersSeen.get(aid);
		
		//If it's the first time we see this agent
		if(oldInfo != null) {
			lastStateZones.get(oldInfo.getLastZone()).remove(oldInfo);
			lastStateZones.get(toRemember.getLastZone()).add(toRemember);
			lastCharactersSeen.remove(aid);
			lastCharactersSeen.put(aid, toRemember);
		//If we have already seen this agent
		}else {
			lastCharactersSeen.put(aid, toRemember);
			lastStateZones.get(toRemember.getLastZone()).add(toRemember);
			deadCharacters.put(aid, false);
		}
	}
	

	/**
	 * Stores that we have seen an agent. (Creates a CharacterInfo from the VisualInfo given)
	 * @param aid AID of this agent
	 * @param seen Visual informations that we have seen
	 * @param time The time when we saw him
	 * @see CharacterInfo
	 */
	public void seeSomeone(AID aid, VisualInfo seen, long time) {
		CharacterInfo toRemember = new CharacterInfo(seen, time);
		seeSomeone(aid, toRemember);	
	}
	
	/**
	 * @param me my character
	 * @return a list of my closest enemies
	 */
	public List<CharacterInfo> getClosestEnemy(Character me){
		List<CharacterInfo> toReturn = new ArrayList<CharacterInfo>();
		List<CharacterInfo> toRemove = new ArrayList<CharacterInfo>();
		for(Map.Entry<AID, CharacterInfo> entry : lastCharactersSeen.entrySet()) {
			if(entry.getValue().getTeam().enemy().equals(me.team)) {
				toReturn.add(entry.getValue());
			}
		}
		int min = 3;
		for(CharacterInfo enemies : toReturn) {
			int dist = me.getCurrentZone().distance(enemies.getLastZone());
			if(dist < min)
				min = dist;
		}
		for(CharacterInfo enemies : toReturn) {
			int dist = me.getCurrentZone().distance(enemies.getLastZone());
			if(dist > min)
				toRemove.add(enemies);
		}
		for(CharacterInfo remove : toRemove)
			toReturn.remove(remove);
		return toReturn;
		
	}
	
	/**
	 * @param me my character
	 * @return a list of my closest allies
	 */
	public List<CharacterInfo> getClosestAlly(Character me){
		List<CharacterInfo> toReturn = new ArrayList<>();
		List<CharacterInfo> toRemove = new ArrayList<>();
		for(Map.Entry<AID, CharacterInfo> entry : lastCharactersSeen.entrySet()) {
			if(entry.getValue().getTeam().equals(me.team)) {
				toReturn.add(entry.getValue());
			}
		}
		int min = 3;
		for(CharacterInfo allies : toReturn) {
			int dist = me.getCurrentZone().distance(allies.getLastZone());
			if(dist < min)
				min = dist;
		}
		for(CharacterInfo allies : toReturn) {
			int dist = me.getCurrentZone().distance(allies.getLastZone());
			if(dist > min)
				toRemove.add(allies);
		}
		for(CharacterInfo remove : toRemove)
			toReturn.remove(remove);
		return toReturn;
	}
	
	/**
	 * Update this memory with the shared memory of an ally.
	 * @param other the shared memory of an ally
	 * @see ShareMemoryBehaviour
	 */
	public void updateGetInfoFromAlly(Memory other) {
		Map<AID, CharacterInfo> lastChars = other.getLastCharactersSeen();
		Map<AID, Boolean> deads = other.deadCharacters;

		//Update dead characters
		for(Map.Entry<AID, Boolean> dead : deads.entrySet()) {
			Boolean myDead = deadCharacters.get(dead.getKey());
			if(myDead != null) {
				if(dead.getValue() && !myDead) {
					deadCharacters.remove(dead.getKey());
					deadCharacters.put(dead.getKey(), true);
				}
			}
		}

		//Update character informations
		for(Map.Entry<AID, CharacterInfo> newChar : lastChars.entrySet()) {
			CharacterInfo mine = lastCharactersSeen.get(newChar.getKey());
			//if the shared memory has newer infos than this memory
			if(mine == null || mine.getTimeInfo() < newChar.getValue().getTimeInfo()) {
				//if I am not sure that he is dead
				if(!iKnowHesDead(newChar.getKey())) {
					seeSomeone(newChar.getKey(), newChar.getValue());
				}
			}
		}
	}
	
	/**
	 * Stores that a character has been killed.
	 * @param characterKilled the AID of the killed character
	 * @see KilledNotificationBehaviour
	 */
	public void killedCharacter(AID characterKilled) {
		deadCharacters.put(characterKilled, true);
		CharacterInfo dead = lastCharactersSeen.remove(characterKilled);
		if(dead != null)
			lastStateZones.get(dead.getLastZone()).remove(dead);
	}
}
