package autorpg;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

/**
 * A behaviour that finds other character in the same zone of the agent at a fixed time frequency.
 * Finding a character means: find his AID + send him a visual information request (look at him).
 * @see Character
 * @see VisualInfoBehaviour
 */
public class LookForCharactersBehaviour extends TickerBehaviour {

	//Generated serial ID
	private static final long serialVersionUID = 8540232479542822313L;

	/**
	 * @param a my agent
	 * @param period time interval for finding other character
	 */
	public LookForCharactersBehaviour(Agent a, long period) {
		super(a, period);
	}

	@Override
	protected void onTick() 
	{
		Character me = ((Character) myAgent);
		me.findNearAllies();
		me.findNearEnemies();
	}

}
