package autorpg;

import jade.core.AID;
import java.util.List;
import java.util.Random;
import java.util.Arrays;

public class Mage extends Character {

	/**
	 * Generated serial
	 */
	private static final long serialVersionUID = 8927597915110559912L;

	@Override
	protected void setup() {
		super.setup();
		maxHp = 200;
		hp = maxHp;
		abilities.put("lightning", new Ability(1750, 40, 0, 1, 1, TARGET.ENEMIES));
		abilities.put("fireball", new Ability(2000, 30, 0, 0, -1, TARGET.ENEMIES));
	}

	@Override
	public char getClassDescriptor() {
		return 'M';
	}

	@Override
	public void startGame() {
		super.startGame();
	}
	
	@Override
	public ZONE computeZone() { // look for ally in memory
		ZONE[] adjacentZones = currentZone.getNext();
		List<CharacterInfo> closeOnes = myMemory.getClosestAlly(this);
		if(closeOnes.isEmpty()){
			return super.computeZone();
		} else {
			if(Arrays.asList(adjacentZones).contains(closeOnes.get(0).getLastZone())){
				return closeOnes.get(0).getLastZone();
			}
		}
		return super.computeZone();
	}

	@Override
	public AID[] computeEnemyTarget(int nbTargets) {
		// TODO Auto-generated method stub
		return super.computeEnemyTarget(nbTargets);
	}

	@Override
	public AID[] computeAllyTarget(int nbTargets) {
		return super.computeAllyTarget(nbTargets);
	}
	
	@Override
	public void computeNextAbility() {
		if(nearEnemies.length > 0){ // enemies in zone
			if(nearEnemies.length > nearAllies.length){ // may flee
				int rand = new Random().nextInt(2);
				if(rand == 0){ // 1/3 chance				
					changeNextAbility("move");
				} else { // 2/3 chance
					changeNextAbility("fireball");
				}
			} else { // numerical superiority
				if(nearEnemies.length > 1){
					changeNextAbility("fireball");
				} else {					
					changeNextAbility("lightning");
				}
			}
		} else { // no enemy in zone
			if(nearAllies.length > 1){ // ally in zone
				if(hp < maxHp){ // stays where he is if not enough HP, expecting heal or defense from ally
					changeNextAbility("share");
				} else { // dispos and on the go
					changeNextAbility("move");
				}
			} else { // no ally in zone			
				changeNextAbility("move");
			}
		}
	}
}
