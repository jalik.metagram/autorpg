package autorpg;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.wrapper.AgentContainer;

public class StartAutoRPG extends Agent {

	public static StartAutoRPG INSTANCE = null;

	/**
	 * Generation serial
	 */
	private static final long serialVersionUID = -4547923579153364790L;

	Scanner console;
	AgentContainer container;
	private static double gameSpeed = 1;
	public static double getGameSpeed() {
		return gameSpeed;
	}
	Character[] playerParty;
	Character[] enemyParty;
	int level = 1;
	
	UpdateConsoleBehaviour updater;

	String response = "";
	int valResponded = -1;

	int nbCharacters = 3;
	int nbEnemies= 3;

	/**
	 * Handle game initialization and several components setup
	 */
	@Override
	protected void setup() {
		super.setup();
		INSTANCE = this;
		console = new Scanner(System.in);
		container = getContainerController();
		updater = new UpdateConsoleBehaviour();
		addBehaviour(updater);
		updater.block();

		System.out.println("Welcome to Auto RPG !" + System.lineSeparator() + "Please choose a game speed : "
					+ System.lineSeparator() + "1 : Very fast"
					+ System.lineSeparator() + "2 : Fast"
					+ System.lineSeparator() + "3 : Normal"
					+ System.lineSeparator() + "4 : Slow"
				);
		String response = console.nextLine();
		int valResponded = checkString(response, 4);
		switch(valResponded) {
			case 1:
				gameSpeed = 0.5;
				break;
			case 2:
				gameSpeed = 0.75;
				break;
			case 3:
				gameSpeed = 1;
				break;
			default:
				gameSpeed = 1.5;
				break;
		}
		System.out.println(System.lineSeparator() + "Thank you ! Make your party :");
		launchGame(level);
	}


	public static final String os = System.getProperty("os.name");
	int nbMessage = 0;

	/**
	 * Handle messages displayed on the console while the game is playing
	 * @param message Message to display
	 */
	public void updateConsoleMessage(String message) {
		++nbMessage;
		if(nbMessage > 5) {
			mesConsole = mesConsole.substring(mesConsole.indexOf(System.lineSeparator())+1);
		}
		mesConsole += Integer.toString(nbMessage) + " : " + message + System.lineSeparator();
	}
	private String mesConsole = "";

	/**
	 * Handle console display, game over check and next level setup
	 */
	public void updateConsole() {
		try {
	        StringBuilder toPrint = new StringBuilder();
			ArrayList<Character> northZone = new ArrayList<>();
			ArrayList<Character> southZone = new ArrayList<>();
			ArrayList<Character> eastZone = new ArrayList<>();
			ArrayList<Character> westZone = new ArrayList<>();
			int nbPlayerDead = 0;
			int nbEnemyDead = 0;
			for (Character current : playerParty) {
				if (current.isReady()) {
					switch (current.currentZone) {
						case NORTH:
							northZone.add(current);
							break;
						case SOUTH:
							southZone.add(current);
							break;
						case WEST:
							westZone.add(current);
							break;
						case EAST:
							eastZone.add(current);
							break;
					}
				} else {
					++nbPlayerDead;
				}
			}
			for (Character current : enemyParty) {
				if(current.isReady()) {
					switch (current.currentZone) {
						case NORTH:
							northZone.add(current);
							break;
						case SOUTH:
							southZone.add(current);
							break;
						case WEST:
							westZone.add(current);
							break;
						case EAST:
							eastZone.add(current);
							break;
					}
				}else {
					++nbEnemyDead;
				}
			}

			//CHECK GAME OVER
			if (nbEnemyDead == nbEnemies) {
				// Kill all agents still alive
				for (Character current : playerParty) {
					if (current.isReady()) {
						try {
							DFService.deregister(current);
						} catch (FIPAException e) {
							System.err.println("CANT DEREGISTER");
							e.printStackTrace();
						}
						current.doDelete();
					}
				}
				nbMessage = 0;
				mesConsole = "";
				updater.block();
				System.out.println("Congrats, you won !"+System.lineSeparator()+"Press 1 to go to next level or 2 to quit");
				do {
					response = console.nextLine();
					valResponded = checkString(response, 2);
				}while(valResponded == -1);
				if(valResponded == 1)
				{
					++level;
					launchGame(level);
				}else {
					System.out.println("Bye !");
					stopGame();
				}
			}
			if (nbPlayerDead == nbCharacters) {
				for (Character current : enemyParty) {
					if (current.isReady()) {
						try {
							DFService.deregister(current);
						} catch (FIPAException e) {
							System.err.println("CANT DEREGISTER");
							e.printStackTrace();
						}
						current.doDelete();
					}
				}
				nbMessage = 0;
				mesConsole = "";
				updater.block();
				System.out.println("Too bad, you lost."+System.lineSeparator()+"Press 1 to go to restart level or 2 to quit");
				do {
					response = console.nextLine();
					valResponded = checkString(response, 2);
				}while(valResponded == -1);
				if(valResponded == 1)
				{
					launchGame(level);
				}else {
					System.out.println("Bye !");
					stopGame();
				}
			}

			//DISPLAY
			toPrint.append("\t\t\t\tNORTH").append(System.lineSeparator());
			for (Character current : northZone) {
				toPrint.append("\t\t\t\t").append(current.getLocalName()).append(" (").append(current.hp).append("/").append(current.maxHp).append(")").append(System.lineSeparator());
			}
			for(int i = northZone.size() ; i <= 6 ; ++i)
				toPrint.append(System.lineSeparator());
			toPrint.append("\t /\t\t\t\t\t\t\\").append(System.lineSeparator()).append("\tWEST\t\t\t\t\t\tEAST").append(System.lineSeparator());
			int westsize = westZone.size();
			int eastsize = eastZone.size();
			for(int i = 0 ; i < (Math.max(eastsize, westsize)); ++i) {
				toPrint.append('\t');
				if(i < westsize) {
					Character current = westZone.get(i);
					if(current.isAlive()) {
						toPrint.append(current.getLocalName()).append(" (").append(current.hp).append("/").append(current.maxHp).append(")");
					}
				}
				if(i < eastsize) {
					toPrint.append("\t\t\t\t\t\t");
					Character current = eastZone.get(i);
					if(current.isAlive()) {
						toPrint.append(current.getLocalName()).append(" (").append(current.hp).append("/").append(current.maxHp).append(")");
					}
				}
				toPrint.append(System.lineSeparator());
			}
			for(int i = Math.max(eastZone.size(), westsize) ; i <= 6 ; ++i)
				toPrint.append(System.lineSeparator());
			System.out.println("\t\t\t\t \\  /");
			System.out.println("\t\t\t\tSOUTH");
			toPrint.append("\t\t\t\t \\  /").append(System.lineSeparator()).append("\t\t\t\tSOUTH").append(System.lineSeparator());
			for (Character current : southZone) {
				toPrint.append("\t\t\t\t").append(current.getLocalName()).append(" (").append(current.hp).append("/").append(current.maxHp).append(")").append(System.lineSeparator());
			}
			toPrint.append(System.lineSeparator());
			if(!mesConsole.equals("")) {
				toPrint.append(System.lineSeparator()).append(mesConsole);
			}
			if (os.contains("Windows"))
	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	        else
	            Runtime.getRuntime().exec("clear");
			
			System.out.println(toPrint.toString());
		} catch (Exception e) {
			e.printStackTrace();
			stopGame();
		}
	}

	/**
	 * Stop the game and close the console
	 */
	private void stopGame() {
		console.close();
		doDelete();
	}

	/**
	 * Handle ally party creation by the user
	 * @param nbChar number of characters in the party
	 */
	private void createPlayerParty(int nbChar){
		playerParty = new Character[nbChar];
		response = "";
		valResponded = -1;
		System.out.println("You have to choose " + nbChar + " characters !");
		for(int i = 1 ; i <= nbChar ; ++i) {
			System.out.println("Choose your character number " + i + " among :");
			do {
				System.out.println("1 - Barbarian\n2 - Mage\n3 - Healer");
				response = console.nextLine();
				valResponded = checkString(response, 3);
			}while(valResponded == -1);
			Character toAdd;
			if(valResponded == 1) {
				toAdd = new Barbarian();
			}else if(valResponded == 2) {
				toAdd = new Mage();
			}
			else {
				toAdd = new Healer();
			}
			playerParty[i-1] = toAdd;
			System.out.println("Added a " + toAdd.getClass().getSimpleName() + " to your team !");
		}
	}

	/**
	 * Handle enemy party creation
	 * @param nbEnemy number of enemies to fight
	 * @param fixParty tell if the enemy party is generated with a certain pattern or randomly
	 */
	private void createEnemyParty(int nbEnemy, boolean fixParty){
		if (fixParty) {
			createFixEnemyParty(nbEnemy);
		} else {
			createRandomEnemyParty(nbEnemy);
		}
		System.out.println("You're facing against :");
		for(int i = 0 ; i < nbEnemy ; ++i) {
			System.out.println("A " + enemyParty[i].getClass().getSimpleName());
		}
		System.out.println();
	}

	/**
	 * Create an enemy party with a given form (1 barbarian, 2 mages, 1 healer, repeat)
	 * @param nbEnemy number of enemies to fight
	 */
	private void createFixEnemyParty(int nbEnemy){
		enemyParty = new Character[nbEnemy];
		for(int i = 0 ; i < nbEnemy ; i++) {
			switch (i % 4) {
				case 0:
					enemyParty[i] = new Barbarian();
					break;
				case 1:
				case 2:
					enemyParty[i] = new Mage();
					break;
				case 3:
					enemyParty[i] = new Healer();
					break;
			}
		}
	}

	/**
	 * Create a random enemy party
	 * @param nbEnemy number of enemies to fight
	 */
	private void createRandomEnemyParty(int nbEnemy){
		int rand;
		int[] listEnemy = new int[nbEnemy];
		enemyParty = new Character[nbEnemy];
		boolean allHeal = true;
		while (allHeal) {
			for (int i = 0; i < nbEnemy; i++) {
				rand = new Random().nextInt(6);
				if (rand != 3) allHeal = false;
				listEnemy[i] = rand;
			}
		}
		for (int i = 0 ; i < nbEnemy ; i++) {
			switch (listEnemy[i]) {
				case 1:
				case 2:
					enemyParty[i] = new Mage();
					break;
				case 3:
					enemyParty[i] =  new Healer();
					break;
				default:
					enemyParty[i] =  new Barbarian();
					break;
			}
		}
	}

	/**
	 * Handle parties creation and launch of the level
	 * @param level number of the current level
	 */
	private void launchGame(int level){
		int[] workforce = workforceByLevel(level);
		nbCharacters = workforce[0];
		nbEnemies = workforce[1];

		//for enemy party to have a fix form, for test purpose
		//createEnemyParty(nbEnemies, true);
		//for enemy party to be random
		createEnemyParty(nbEnemies, false);

		createPlayerParty(nbCharacters);

		//Add agents in the container
		try {
			for(int i = 0 ; i < playerParty.length ; ++i) {
				playerParty[i].setTeam(TARGET.ALLIES);
				container.acceptNewAgent(playerParty[i].getClassDescriptor() + "A" + (i + 1), playerParty[i]).start();
				}
			for(int i = 0 ; i < enemyParty.length ; ++i) {
				enemyParty[i].setTeam(TARGET.ENEMIES);
				container.acceptNewAgent(enemyParty[i].getClassDescriptor() + "E" + (i + 1), enemyParty[i]).start();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			stopGame();
		}

		response = "";
		valResponded = -1;
		System.out.println("Start Game ?\n1 - Yes\n2 - No");
		do {
			response = console.nextLine();
			valResponded = checkString(response, 2);
		}while(valResponded == -1);
		if(valResponded == 1)
		{
			for(Character player : playerParty) {
				player.startGame();
			}
			for(Character enemy : enemyParty) {
				enemy.startGame();
			}
			updater.restart();

		}else {
			System.out.println("Bye !");
			stopGame();
		}
	}

	/**
	 * Give how many members there is in both parties for a given level
	 * @param level Number of the level
	 * @return An array with two integers being the numbers of allies and the number of enemies
	 */
	private int[] workforceByLevel(int level){
	 switch (level) {
			case 1:
				return new int[]{2, 1};
			case 2:
				return new int[]{3, 2};
			case 3:
				return new int[]{3, 3};
			case 4:
				return new int[]{3, 4};
			default:
				return new int[2];
		}
	}

	/**
	 * Check if a string is an integer between 0 and maxValue
	 * @param toCheck String to verify
	 * @param maxValue Maximum value possible
	 * @return -1 if the string do not respect rules, its value as integer otherwise
	 */
	private int checkString(String toCheck, int maxValue) {
		try {
			int val = Integer.parseInt(toCheck);
			if(val <= 0) {
				System.err.println(toCheck + " is zero or negative, please enter a positive number");
				return -1;
			}else if(val > maxValue) {
				System.err.println(toCheck + " is greater than " + maxValue + ". Please enter a number beetween 1 and " + maxValue);
				return -1;
			}
			return val;
		}catch(NumberFormatException e) {
			System.err.println("Can't read " + toCheck + " as a number. Please enter a number beetween 1 and " + maxValue);
		}
		
		return -1;
	}
	
}
