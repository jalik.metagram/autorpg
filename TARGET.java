package autorpg;

public enum TARGET {
	ALLIES,ENEMIES,NONE;

	public TARGET enemy() {
		switch (this) {
			case ALLIES:
				return ENEMIES;
			case ENEMIES:
				return ALLIES;
			default:
				return NONE;
		}
	}
}
