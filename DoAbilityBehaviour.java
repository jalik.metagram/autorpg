package autorpg;

import java.io.IOException;
import java.util.Random;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * A cyclic behaviour that allows an agent to do one ability, 
 * wait for the duration of that ability,
 * and then do another ability again, etc.... 
 * @see Ability
 */
public class DoAbilityBehaviour extends CyclicBehaviour 
{
	/**
	 * Generated Serial ID
	 */
	private static final long serialVersionUID = 1183191679558747276L;

	
	/**
	 * The next ability to do
	 */
	protected Ability nextAbility;

	
	/**
	 * The last time an ability has been done
	 */
	protected long lastTime = 0;


	/**
	 * The time to wait before doing another ability
	 */
	protected long toWait = 0;
	

	/**
	 * @param startingAbility the first ability to do 
	 */
	public DoAbilityBehaviour(Ability startingAbility) {
		super();
		nextAbility = startingAbility;
		toWait = ((long)(1000 * StartAutoRPG.getGameSpeed())) + new Random().nextInt(100);
	}
	

	public Character getMyCharacter() {
		return (Character) myAgent;
	}
	

	@Override
	public void action() 
	{
		//We check if we are not waiting for the end of the last ability
		long currentTime = System.currentTimeMillis();
		if(lastTime == 0)
			lastTime = currentTime;
		if(lastTime + toWait < currentTime) 
		{
			if(getMyCharacter().isReady()) 
			{
				getMyCharacter().computeNextAbility();
				switch(nextAbility.targetType) 
				{
					//this ability targets ennemies, so it deal damages to them
					case ENEMIES:
						AID[] targets = getMyCharacter().computeEnemyTarget(nextAbility.nbTargets);
						for(int i = 0 ; i < targets.length ; ++i) 
						{
							dealDamage(targets[i], nextAbility.nbLifePointsModified);
						}						
						break;

					//this ability target allies, so it heals them
					case ALLIES:
						AID[] healed = getMyCharacter().computeAllyTarget(nextAbility.nbTargets);
						for(int i = 0 ; i < healed.length ; ++i) 
						{
							dealHealing(healed[i], nextAbility.nbLifePointsModified);
						}
						break;

					//this ability target no team, so it's either a move or a share memory with allies
					case NONE:
						//move ability
						if(nextAbility.nbZoneMove == 1) 
						{
							ZONE next = getMyCharacter().computeZone();
							getMyCharacter().move(next);
						}
						//share memory ability
						else 
						{
							AID[] toShare = getMyCharacter().computeAllyTarget(nextAbility.nbTargets);
							for(int i = 0 ; i < toShare.length ; ++i) 
							{
								shareMemory(toShare[i]);
							}
						}
						break;
				}
				lastTime = currentTime;
				//Wait for the duration of the ability
				block(toWait);
			}
		}
		else {
			//The agent received a message that will be processed by another behaviour
			block(lastTime + toWait - currentTime);
		}
	}
	

	/**
	 * Helper function to update next ability and time to wait 
	 * @param newAbility the next ability to do
	 */
	public void switchAbility(Ability newAbility) {
		nextAbility = newAbility;
		toWait = ((long)(nextAbility.millisCooldown * StartAutoRPG.getGameSpeed()));
	}


	/**
	 * Send attack message to an enemy agent 
	 * @param target AID of the agent to attack
	 * @param damages the amount of life the target will lose
	 * @see ReceiveDamageBehaviour
	 */
	public void dealDamage(AID target, int damages) {   
		ACLMessage atkMsg = new ACLMessage(ACLMessage.INFORM);
		atkMsg.setOntology(ReceiveDamageBehaviour.ONTOLOGY);
        atkMsg.setContent(Integer.toString(damages));
		atkMsg.addReceiver(target);
        myAgent.send(atkMsg);
	}


	/**
	 * Send a message to share memory with an ally agent.
	 * The agent first sends his memory, and then his ally will reply with his own memory.
	 * Memory is sent as a serializable Memory class.
	 * @param target The ally to share my memory with
	 * @see ShareMemoryBehaviour
	 * @see Memory
	 */
	public void shareMemory(AID target) {
		try {
			Character myChar = (Character) myAgent;
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.setOntology(ShareMemoryBehaviour.ONTOLOGY);
			msg.setContentObject(myChar.getMyMemory());
			msg.addReceiver(target);
			myAgent.send(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Send a heal message to an ally.
	 * @param target the ally agent that will be healed
	 * @param heal the amount of life the target will recover
	 */
	public void dealHealing(AID target, int heal) {
		ACLMessage healMsg = new ACLMessage(ACLMessage.INFORM);
		healMsg.setOntology(ReceiveHealBehaviour.ONTOLOGY);
		healMsg.setContent(Integer.toString(heal));
		healMsg.addReceiver(target);
		myAgent.send(healMsg);
	}

}
