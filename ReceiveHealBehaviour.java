package autorpg;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * This behaviours receive heal messages from other agent.
 * 
 * When an agent wants to heal this agent, he sends a message with the amount of heal points.
 * After reception, this agent will increase its hit points (or life points) by the amount of heal received.
 */
public class ReceiveHealBehaviour extends CyclicBehaviour {

    //Generated Serial ID
    private static final long serialVersionUID = -5343650854987385573L;

    //Ontology is used as the category of these messages
    public static final String ONTOLOGY = "heal";	

    //Message templates are used to filter messages by ontogy and performative
	private final MessageTemplate performativeTemp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
	private final MessageTemplate ontologyTemp = MessageTemplate.MatchOntology(ReceiveHealBehaviour.ONTOLOGY);

    public void action() 
    {
        //Try to receive a heal message ...
        ACLMessage message = myAgent.receive(MessageTemplate.and(performativeTemp, ontologyTemp));
        if (message != null) 
        {
            //Increase my hit points by the amount of heal received
            Character myChar = (Character) myAgent;
            myChar.receiveHeal(Integer.parseInt(message.getContent()));

            //Add notification to the console
            StartAutoRPG.INSTANCE.updateConsoleMessage(myAgent.getLocalName() + " received " + message.getContent() + " heal");
        }
        //Wait for other heal messages
        block();
    }
}
