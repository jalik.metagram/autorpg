package autorpg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import jade.core.AID;
import jade.core.Agent;
import jade.wrapper.AgentState;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;

public abstract class Character extends Agent {

	/**
	 * Generated serial
	 */
	private static final long serialVersionUID = -4695535772685051174L;
	
	protected int hp; 
	protected int maxHp;
	protected Map<String, Ability> abilities;
	protected ZONE currentZone;
	protected TARGET team;
	protected DFAgentDescription DFdescription;
	protected AID[] nearEnemies;
	protected AID[] nearAllies;
	protected DoAbilityBehaviour abilitiesDoing = null;
	protected Memory myMemory;

	/**
	 * Handle initialization of a generic character, with basic abilities
	 */
	@Override
	protected void setup() {
		super.setup();
		abilities = new HashMap<String, Ability>();
		abilities.put("move", new Ability(750, 0, 1,0, 0, "Moving", TARGET.NONE));
		abilities.put("share", new Ability(250, 0, 0, 0, 1, TARGET.NONE));
		DFdescription = new DFAgentDescription();
		nearEnemies = null;
		myMemory = new Memory();
		registerToDF();
	}

	/**
	 * Remember that an enemy has been killed
	 * @param enemyKilled AID of the now-dead enemy
	 */
	public void notifyKilled(AID enemyKilled) {
		myMemory.killedCharacter(enemyKilled);
	}

	/**
	 * Handle the damages taken by the character from an enemy attack, which will reduce its "health points" (its life force)
	 * Kill the agent if its health reaches 0
	 * @param deg the amount of damage received
	 */
	public void takeDamage(int deg) {
		hp -= deg;
		if(hp <= 0) {
			try {
				DFService.deregister(this);
			} catch (FIPAException e) {
				System.err.println("CANT DEREGISTER");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Handle the healing received by the character from an allied healer. Its health will rise by the given amount, but it cannot exceed its maximum health maxHp
	 * @param heal the amount of healing received
	 */
	public void receiveHeal(int heal) {
		hp += heal;
		if (hp >= maxHp) hp = maxHp;
	}

	/**
	 * Give which zone the character will go next. By default it's random
	 * @return a zone adjacent to the one the character is in
	 */
	public ZONE computeZone() {
		return currentZone.getRandomNext();
	}

	/**
	 * Give the enemy that will be targeted by the next ability
	 * @param nbTargets number of targets that the ability can touch
	 * @return an array of AIDs corresponding to the targets
	 */
	public AID[] computeEnemyTarget(int nbTargets) {
		if(nbTargets == -1)
			return nearEnemies;
		ArrayList<AID> enemiesPossible = new ArrayList<AID>();
		for(int i = 0 ; i < nearEnemies.length ; ++i) {
			enemiesPossible.add(nearEnemies[i]);
		}
		AID[] toReturn = new AID[Math.min(nbTargets, enemiesPossible.size())];
		for(int i = 0 ; i < nbTargets && !enemiesPossible.isEmpty(); ++i) {
			toReturn[i] = enemiesPossible.get(new Random().nextInt(enemiesPossible.size()));
			enemiesPossible.remove(toReturn[i]);
		}
		return toReturn;
	}

	/**
	 * Give the ally that will be targeted by the next ability
	 * @param nbTargets number of targets that the ability can touch
	 * @return an array of AIDs corresponding to the targets
	 */
	public AID[] computeAllyTarget(int nbTargets) {
		if(nbTargets == -1)
			return nearAllies;
		ArrayList<AID> alliesPossible = new ArrayList<AID>();
		for(int i = 0 ; i < nearAllies.length ; ++i) {
			alliesPossible.add(nearAllies[i]);
		}
		AID[] toReturn = new AID[Math.min(nbTargets, alliesPossible.size())];
		for(int i = 0 ; i < nbTargets && !alliesPossible.isEmpty(); ++i) {
			toReturn[i] = alliesPossible.get(new Random().nextInt(alliesPossible.size()));
			alliesPossible.remove(toReturn[i]);
		}
		return toReturn;
	}

	/**
	 * Choose the next ability that will be used. Be default it's move
	 */
	public void computeNextAbility() {
		changeNextAbility("move");
	}

	/**
	 * Change the next ability that will be used
	 * @param nameNext Name of the chosen ability
	 */
	public void changeNextAbility(String nameNext) {
		if(abilitiesDoing != null) {
			abilitiesDoing.switchAbility(abilities.get(nameNext));
		}
	}

	/**
	 * Handle the movement of the character from one zone to another and send a message to the console
	 * @param newZone the zone where the character will move to
	 */
	public void move(ZONE newZone) {
		DFdescription.removeOntologies(currentZone.toString());
		DFdescription.addOntologies(newZone.toString());
		String oldZone = currentZone.toString();
		currentZone = newZone;
		try {
		  DFService.modify(this, DFdescription);
		} catch (FIPAException e) {
		  e.printStackTrace();
		}
		StartAutoRPG.INSTANCE.updateConsoleMessage(getLocalName() + " moved from " + oldZone + " to " + newZone);
	}

	/**
	 * Link the behaviours that will be used
	 */
	public void startGame()
	{
		addBehaviour(new ReceiveDamageBehaviour());
		addBehaviour(new ReceiveHealBehaviour());
		this.abilitiesDoing = new DoAbilityBehaviour(abilities.get("move")); 
		addBehaviour(abilitiesDoing);
		addBehaviour(new VisualInfoBehaviour());
		addBehaviour(new LookForCharactersBehaviour(this, 50));
		addBehaviour(new ShareMemoryBehaviour());
		addBehaviour(new KilledNotificationBehaviour());
	}

	/**
	 * Set the team of the character and change its position to the team start zone
	 * @param team a target representing the character's team
	 */
	protected void setTeam(TARGET team) {
		this.team = team;
		if(this.team == TARGET.ALLIES)
			currentZone = ZONE.NORTH;
		else if(this.team == TARGET.ENEMIES) {
			currentZone = ZONE.SOUTH;
		}
		else {
			System.err.println("Team sent to " + getName() + " NONE : can't use this agent");
			return;
		}
	}

	/**
	 * Register the agent to the DF
	 */
	protected void registerToDF() {
		DFdescription.setName(getAID());
		DFdescription.addOntologies(currentZone.toString());
		DFdescription.addOntologies(team.toString());
		try {
		  DFService.register(this, DFdescription);
		} catch (FIPAException e) {
		  e.printStackTrace();
		}
	}

	/**
	 * Get the enemies near the character
	 * @return an array of AIDs corresponding to the enemies
	 */
	public AID[] getNearEnemies() {
		return nearEnemies;
	}

	/**
	 * Get the allies near the character, including itself
	 * @return an array of AIDs corresponding to the allies
	 */
	public AID[] getNearAllies() {
		return nearAllies;
	}

	/**
	 * Find the enemies in the same zone as the character
	 */
	protected void findNearEnemies() {
        DFAgentDescription enemyTemplate = new DFAgentDescription();
		enemyTemplate.addOntologies(currentZone.toString());
		enemyTemplate.addOntologies(team.enemy().toString());

		ACLMessage lookMessage = new ACLMessage(ACLMessage.REQUEST);
		lookMessage.setOntology(VisualInfoBehaviour.ONTOLOGY);
		
        DFAgentDescription[] result;
        try {
			result = DFService.search(this, enemyTemplate);
		  	if (result.length > 0) {
				nearEnemies = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					nearEnemies[i] = result[i].getName();
					lookMessage.addReceiver(result[i].getName());
				}
				send(lookMessage);
			}
			else {
				nearEnemies = new AID[0];
			}
        } 
        catch (FIPAException e) {
          e.printStackTrace();
        }
	}

	/**
	 * Find the allies in the same zone as the character, including himself
	 */
	protected void findNearAllies() {
		DFAgentDescription allyTemplate = new DFAgentDescription();
		allyTemplate.addOntologies(currentZone.toString());
		allyTemplate.addOntologies(team.toString());

		ACLMessage lookMessage = new ACLMessage(ACLMessage.REQUEST);
		lookMessage.setOntology(VisualInfoBehaviour.ONTOLOGY);
		
        DFAgentDescription[] result;
        try {
			result = DFService.search(this, allyTemplate);
		  	if (result.length > 0) {
				nearAllies = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					nearAllies[i] = result[i].getName();
					lookMessage.addReceiver(result[i].getName());
				}
				send(lookMessage);
			}
			else {
				nearAllies = new AID[0];
			}
        } 
        catch (FIPAException e) {
          e.printStackTrace();
        }
	}

	/**
	 * Get the character's memory
	 * @return the character's memory
	 */
	public Memory getMyMemory() {
		return myMemory;
	}

	/**
	 * Get the character's current health points
	 * @return the character's current health points
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * Get a dictionary of the character's abilities
	 * @return a dictionary of the character's abilities
	 */
	public Map<String, Ability> getAbilities() {
		return abilities;
	}

	/**
	 * Get the character's current zone
	 * @return the character's current zone
	 */
	public ZONE getCurrentZone() {
		return currentZone;
	}

	/**
	 * Get the character's team
	 * @return the character's team
	 */
	public TARGET getTeam() {
		return team;
	}

	/**
	 * Tell if the agent is alive or not
	 * @return true if the agent is alive
	 */
	public boolean isReady() {
		return super.getAgentState().getValue() != AgentState.cAGENT_STATE_DELETED;
	}

	/**
	 * Get the character's maximum health points
	 * @return the character's maximum health points
	 */
	public int getMaxHp() {
		return maxHp;
	}
	
	public abstract char getClassDescriptor();

	/**
	 * Add to the memory the fact that another character has been seen
	 * @param aid the other character's AID
	 * @param info the other character's info
	 * @param time how long ago he was seen
	 */
	public void addVisualInfo(AID aid, VisualInfo info, long time) {
		myMemory.seeSomeone(aid, info, time);
	}
}
