package autorpg;

import java.io.Serializable;

public class Ability implements Serializable {

	/**
	 * Generated seial
	 */
	private static final long serialVersionUID = 6444246106732786244L;

	/**
	 * Le nombre de points de vies modifi�s
	 * Pour attaque = nb points de vies enlev�s
	 * Pour soin = nb points de vies ajout�s
	 */
	public final int nbLifePointsModified;
	
	/**
	 * La distance parcourue par l'abilit�
	 * 0 = ne se d�place pas
	 * 1 = d�placement d'une zone
	 * 2 = ...
	 * -1 = d�placement infini 
	 */
	public final int nbZoneMove;
	
	/**
	 * La distance d'attaque
	 * 0 = Melee
	 * 1 = � une zone de distance
	 * 2 = � 2 zones de distance
	 * -1 = infini
	 */
	public final int distance;
	
	/**
	 * Nombre de cibles de l'abilit�
	 * 0 = aucun (d�placement par exemple)
	 * 1 = Premi�re cible trouv�e
	 * 2 = deux premi�res cibles trouv�es
	 * -1 = toutes les cibles
	 */
	public final int nbTargets;
	
	/**
	 * Message to send.
	 * Set to "" if no message has to be sent
	 */
	public final String messageSent;
	
	/**
	 * Type de la cible
	 */
	public final TARGET targetType;

	public final long millisCooldown;
	
	public Ability(long millisCooldown, int nbLifePointsModified, int nbZoneMove, int distance, int nbTargets, String messageSent,
			TARGET targetType) {
		super();
		this.millisCooldown = millisCooldown;
		this.nbLifePointsModified = nbLifePointsModified;
		this.nbZoneMove = nbZoneMove;
		this.distance = distance;
		this.nbTargets = nbTargets;
		this.messageSent = messageSent;
		this.targetType = targetType;
	}
	
	public Ability(long millisCooldown, int nbLifePoints, int nbZoneMove, int distance, int nbTargets, TARGET targetType) {
		this(millisCooldown, nbLifePoints, nbZoneMove, distance, nbTargets, "", targetType);
	}
	
	@Override
	public String toString() {
		return targetType.name() + " modify by " + Integer.toString(nbLifePointsModified) + " and moves " + Integer.toString(nbZoneMove) + " sending message " + messageSent + " ; waiting for " + Long.toString(millisCooldown) + " milliseconds";
	}
}
